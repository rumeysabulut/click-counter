/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import { StyleSheet, View, Button, Text} from 'react-native';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {count: 0}

    this.clickHandler = this.clickHandler.bind(this);
  }

  clickHandler = () => {
    this.setState(({ count }) => ({
      count: count + 1
    }));
  };
  
  render() {
    return (
      <View style={styles.container}>
        <Button title={'Press'} onPress={() => {this.clickHandler()}}></Button>
        <View style={styles.counterStyle}>
          <Text style={styles.textStyle}>{this.state.count}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  counterStyle: {
    backgroundColor: '#FF5733',
    width: 50,
    alignItems: 'center',
  },
  textStyle: {
    fontSize: 45
  }
});
